package edu.szyrek.faxus.filesystem;

public class NoSuchDependencyException extends Exception {
	public NoSuchDependencyException(String s){
		super(s);
	}
}