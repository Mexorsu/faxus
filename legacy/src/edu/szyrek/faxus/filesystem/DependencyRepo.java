package edu.szyrek.faxus.filesystem;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.szyrek.faxus.PreCompiledDependency;

public class DependencyRepo {
	private Path baseDir;
	private static String defaultBaseDir = System.getProperty("user.home")+File.separator+".nexus";
	
	public DependencyRepo() throws IOException {
		this(defaultBaseDir);
	}
	
	public DependencyRepo(String path) throws IOException {
		baseDir = Paths.get(path);
		if (!Files.exists(baseDir)) {
			Files.createDirectories(baseDir);
		}
	}
	public File getPath(PreCompiledDependency dep) {
		Path depDirPath = Paths.get(baseDir+File.separator+dep.getDependencyPath());
		return depDirPath.toFile();
	}

	public Path getBaseDir() {
		return baseDir;
	}
}
