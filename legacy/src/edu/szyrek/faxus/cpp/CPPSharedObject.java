package edu.szyrek.faxus.cpp;

import org.json.JSONObject;

import com.sun.corba.se.impl.util.Version;

import edu.szyrek.faxus.Architecture;
import edu.szyrek.faxus.System;

public class CPPSharedObject extends CPPDynamicLib {
	private int majorVersion;
	private int minorVersion;
	private int releaseNumber;
	public CPPSharedObject(String name, Architecture arch, System system,
			Compiler compiler, int major, int minor, Integer release, String stdLibVersion, boolean debug) {
		super(name, arch, system, compiler, major+"."+"minor"+(release==null ? "": "."+release), stdLibVersion, debug);
		this.majorVersion = major;
		this.minorVersion = minor;
		this.releaseNumber = release;
	}
	
	public CPPSharedObject(JSONObject json) {
		super(json);
		String[] verSplit = this.getVersion().split("\\.");
		this.setMajorVersion(Integer.parseInt(verSplit[0]));
		this.setMinorVersion(Integer.parseInt(verSplit[1]));
		this.setReleaseNumber((verSplit.length > 2 ? Integer.parseInt(verSplit[2]) : null));
	}
	public int getMajorVersion() {
		return majorVersion;
	}
	public void setMajorVersion(int majorVersion) {
		this.majorVersion = majorVersion;
	}
	public int getMinorVersion() {
		return minorVersion;
	}
	public void setMinorVersion(int minorVersion) {
		this.minorVersion = minorVersion;
	}
	public int getReleaseNumber() {
		return releaseNumber;
	}
	public void setReleaseNumber(int releaseNumber) {
		this.releaseNumber = releaseNumber;
	}

	
}
