package edu.szyrek.faxus.cpp;

import org.json.JSONObject;

import edu.szyrek.faxus.Architecture;
import edu.szyrek.faxus.System;

public class CPPDll extends CPPDynamicLib {

	public CPPDll(String name, Architecture arch, System system,
			Compiler compiler, String version, int CRTVersion, boolean debug) {
		super(name, arch, system, compiler, version, CRTVersion+"", debug);
	}
	public CPPDll(JSONObject json) {
		super(json);
		this.setCRTVersion(Integer.parseInt(stdLibVersion));
	}

	public CPPDll() {
	}

	public int getCRTVerion() {
		return Integer.parseInt(stdLibVersion);
	}
	
	public void setCRTVersion(Integer version) {
		this.stdLibVersion = version+"";
	}
}
