package edu.szyrek.faxus.cpp;

import java.io.File;

import org.json.JSONObject;

import edu.szyrek.faxus.Architecture;
import edu.szyrek.faxus.System;

public class CPPDynamicLib extends CPPLibrary {
	public String stdLibVersion;
	public CPPDynamicLib(String name, Architecture arch, System system,
			Compiler compiler, String version, String stdLibVersion, boolean debug) {
		super(name, arch, system,
				compiler, version, debug);
		this.stdLibVersion = stdLibVersion; 
	}
	public CPPDynamicLib() {
	}
	public String getStdLibVersion() {
		return stdLibVersion;
	}
	public void setStdLibVersion(String stdLibVersion) {
		this.stdLibVersion = stdLibVersion;
	}
	public CPPDynamicLib(JSONObject json) {
		super(json);
		this.setStdLibVersion(json.getString("stdlib"));
	}

	@Override
	public String getDependencyPath() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getSystem()+File.separator);
		sb.append(this.getArch()+File.separator);
		sb.append(this.getCompiler()+File.separator);
		sb.append(this.getStdLibVersion()+File.separator);
		sb.append(this.getName()+File.separator);
		sb.append(this.getVersion()+File.separator);
		sb.append("lib");
		sb.append(this.getName());
		sb.append(this.isDebug()?"d":"");
		if (this.getSystem().equals(System.WINDOWS)){
			sb.append(this.getVersion());
			sb.append(".dll");
		} else {
			sb.append(".so.");
			sb.append(this.getVersion());
		}
		return sb.toString();
	}
	@Override
	public JSONObject toRequest() {
		JSONObject json = new JSONObject();
		json.put("class", this.getClass().getCanonicalName());
		json.put("name", this.getName());
		json.put("version", this.getVersion());
		json.put("stdlib", this.getStdLibVersion());
		if (this.getArch()==null) {
			this.setArch(new Architecture(java.lang.System.getProperty("os.arch")));
		}
		json.put("arch", this.getArch().toString());
		if (this.getSystem()==null) {
			this.setSystem(System.fromString(java.lang.System.getProperty("os.name")));
		}
		json.put("system", this.getSystem().toString());
		json.put("compiler", this.getCompiler().toString());
		return json;
	}
	
}
