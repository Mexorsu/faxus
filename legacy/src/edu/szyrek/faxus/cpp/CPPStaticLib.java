package edu.szyrek.faxus.cpp;

import java.io.File;

import org.json.JSONObject;

import edu.szyrek.faxus.Architecture;
import edu.szyrek.faxus.System;


public class CPPStaticLib extends CPPLibrary {

	public CPPStaticLib(String name, Architecture arch, System system,
			Compiler compiler, String version, boolean debug) {
		super(name, arch, system, compiler, version, debug);
	}

	public CPPStaticLib(JSONObject json) {
		super(json);
	}
	
	public CPPStaticLib() {
		super();
	}
	
	public String getDependencyPath() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getSystem()+File.separator);
		sb.append(this.getArch()+File.separator);
		sb.append(this.getCompiler()+File.separator);
		sb.append(this.getName()+File.separator);
		sb.append(this.getVersion()+File.separator);
		sb.append("lib");
		sb.append(this.getName());
		sb.append(this.isDebug()?"d":"");
		if (this.getSystem().equals(System.WINDOWS)){
			sb.append("."+this.getVersion());
			sb.append(".lib");
		} else {
			sb.append("."+this.getVersion());
			sb.append(".a");
		}
		return sb.toString();
	}

	@Override
	public JSONObject toRequest() {
		JSONObject json = new JSONObject();
		json.put("class", this.getClass().getCanonicalName());
		json.put("name", this.getName());
		json.put("version", this.getVersion());
		if (this.getArch()==null) {
			this.setArch(new Architecture(java.lang.System.getProperty("os.arch")));
		}
		json.put("arch", this.getArch().toString());
		if (this.getSystem()==null) {
			this.setSystem(System.fromString(java.lang.System.getProperty("os.name")));
		}
		json.put("system", this.getSystem().toString());
		json.put("compiler", this.getCompiler().toString());
		return json;
	}
	
}
