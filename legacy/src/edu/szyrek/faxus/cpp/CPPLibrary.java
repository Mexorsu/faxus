package edu.szyrek.faxus.cpp;

import org.json.JSONObject;

import edu.szyrek.faxus.Architecture;
import edu.szyrek.faxus.PreCompiledDependency;
import edu.szyrek.faxus.System;

public abstract class CPPLibrary extends PreCompiledDependency {
	private Architecture arch;
	private System system;
	private Compiler compiler;
	private boolean debug;
	
	public CPPLibrary() {
		super();
	}
	
	public CPPLibrary(String name, Architecture arch, System system,
			Compiler compiler, String version, boolean debug) {
		super(name, version);
		this.arch = arch;
		this.system = system;
		this.compiler = compiler;
		this.debug = debug;
	}
	public CPPLibrary(JSONObject json) {
		super(json);
		this.setArch(new Architecture(json.getString("arch")));
		this.setCompiler(Compiler.fromString(json.getString("compiler")));		
		this.setSystem(edu.szyrek.faxus.System.fromString(json.getString("system")));
		if (json.has("debug")) {
			this.setDebug(Boolean.parseBoolean(json.getString("debug")));
		}
	}
	public Architecture getArch() {
		return arch;
	}
	public void setArch(Architecture arch) {
		this.arch = arch;
	}
	public System getSystem() {
		return system;
	}
	public void setSystem(System system) {
		this.system = system;
	}
	public Compiler getCompiler() {
		return compiler;
	}
	public void setCompiler(Compiler compiler) {
		this.compiler = compiler;
	}
	public boolean isDebug() {
		return debug;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
}
