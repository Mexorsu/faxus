package edu.szyrek.faxus.server;

import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import edu.szyrek.faxus.PreCompiledDependency;
import edu.szyrek.faxus.cpp.CPPStaticLib;
import edu.szyrek.myveds.Main;

public class FileServer extends Thread
{
	
	private Properties config = new Properties();
	private static final Logger logger = Logger.getLogger("FileServer");
	private ServerSocket socket;
	private boolean isRunning = false;
    public String repoPath;
	
	public void configureFromFile(String configFilePath) {
		try (InputStream configFile = new FileInputStream(configFilePath)) {
			config.load(configFile);
		} catch (IOException io) {
			logger.log(Level.SEVERE, "Could not load config, reason: "+io.getMessage());
			io.printStackTrace();
		} 
		repoPath = (String)config.getOrDefault("repo", Main.rootDir+File.separator+".faxus/repo");
	}
	
	@Override
	public void start() {
		int port = Integer.parseInt((String)config.getOrDefault("port", "5217"));
		try {
			socket = new ServerSocket(port);
			isRunning = true;
			logger.info("faxus server started on "+port);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Failed to start faxus on "+port);
		}
		super.start();
	}
	
	@Override
	public void run() {
        while(isRunning)
        {
            logger.info("Waiting for Connection ...");
            try {
				FileTransfer transfer = new FileTransfer(socket.accept());
			} catch (IOException e) {
				logger.log(Level.SEVERE,e.getMessage(),  e);
			}
            
        }
	}
	class FileTransfer extends Thread
	{
		private final Logger logger = Logger.getLogger("FileTransfer");
		private DataInputStream input;
		private DataOutputStream output;
		private boolean running;
		private Socket clientSocket;
		
		FileTransfer(Socket socket)
		{
			try
			{
				clientSocket=socket;                        
				input=new DataInputStream(clientSocket.getInputStream());
				output=new DataOutputStream(clientSocket.getOutputStream());
				logger.log(Level.FINE, "Incoming request from "+clientSocket.getInetAddress());
				start(); 
			}
			catch(Exception ex)
			{
			}        
		}
	
		
		void sendDependency()
		{        
			JSONObject jsonRequest;
			try {
				jsonRequest = new JSONObject(input.readUTF());
				PreCompiledDependency dep = new CPPStaticLib(jsonRequest);
				File depFile = new File(repoPath+File.separator+dep.getDependencyPath());
				logger.info("Sending "+depFile.toString());
				if(!depFile.exists())
				{
					output.writeUTF("ERROR: File Not Found");
					return;
				}
				else
				{
					output.writeUTF("READY");
					try (FileInputStream fin=new FileInputStream(depFile)){
						int ch;
						do
						{
							ch=fin.read();
							output.writeUTF(String.valueOf(ch));
						}
						while(ch!=-1);    						
					}catch (Exception ex) {
						throw new IOException("Problems reading file: "+depFile.getAbsolutePath());
					}
					output.writeUTF("File Receive Successfully");                            
				}
			} catch (JSONException|IOException e) {
				logger.log(Level.SEVERE, e.getMessage(), e);
				try {
					output.writeUTF("ERROR: "+e.getMessage());
				} catch (IOException e1) {
					logger.log(Level.SEVERE, e1.getMessage(), e1);
				}
			}
			
		}
		
		void receiveUpload() throws Exception
		{
			JSONObject jsonRequest = new JSONObject(input.readUTF());
			try {
				PreCompiledDependency dep = null;
				dep = (PreCompiledDependency) Class.forName(jsonRequest.getString("class")).getDeclaredConstructor(JSONObject.class).newInstance(jsonRequest);
				File depFile = new File(repoPath + File.separator + dep.getDependencyPath());
				if (!depFile.getParentFile().exists()) {
					Files.createDirectories(Paths.get(depFile.getParent()));
				}
				output.writeUTF("READY");
				try (FileOutputStream fout=new FileOutputStream(depFile)){
					int ch;
					String temp;
					do
					{
						temp=input.readUTF();
						ch=Integer.parseInt(temp);
						if(ch!=-1)
						{
							fout.write(ch);                    
						}
					}while(ch!=-1);
					output.writeUTF("SUCCESS");
				} catch (Exception e) {
					logger.log(Level.SEVERE, e.getMessage(), e);
					output.writeUTF("ERROR: failed to save dependency "+dep.toString()+ " on fexus server");
				}

			} catch (Exception e){
				logger.log(Level.SEVERE, e.getMessage(), e);
				output.writeUTF("ERROR: could not parse dependency of class"+jsonRequest.getString("class"));
			}
			
		}
		
		
		public void run()
		{
			running = true;
			while(running)
			{
				try
				{
					String command = input.readUTF();
					
					if (command.matches("GETDEP")){
						logger.log(Level.FINE, "PUT Request from "+clientSocket.getInetAddress());               
						sendDependency();
						continue;
					}
					else if (command.matches("PUT")){
						logger.log(Level.FINE, "PUT Request from "+clientSocket.getInetAddress());               
						receiveUpload();
						continue;
					}
					else if(command.matches("DISCONNECT"))
					{
						System.out.println("Client "+clientSocket.getInetAddress()+" disconnected");
						running = false;
					}
				}
				catch(Exception ex)
				{
				}
			}
		}
	}
}
