package edu.szyrek.faxus;

import org.json.JSONObject;

public abstract class PreCompiledDependency {
	private String name;
	private String version;
	private String clazz;
	
	public PreCompiledDependency() {}
	public PreCompiledDependency(JSONObject request){
		super();
		this.setName(request.getString("name"));
		this.setVersion(request.getString("version"));
		this.clazz = request.getString("class");
	}
	
	public PreCompiledDependency(String name, String version) {
		super();
		this.name = name;
		this.version = version;
		this.clazz = this.getClass().getSimpleName();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getClazz() {
		return clazz;
	}
	public abstract JSONObject toRequest();
	
	public abstract String getDependencyPath();
}
