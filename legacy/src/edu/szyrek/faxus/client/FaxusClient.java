package edu.szyrek.faxus.client;

import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONObject;

import edu.szyrek.faxus.PreCompiledDependency;
import edu.szyrek.faxus.cpp.CPPStaticLib;
import edu.szyrek.faxus.filesystem.DependencyRepo;
import edu.szyrek.faxus.filesystem.NoSuchDependencyException;


public class FaxusClient
{
	private static final String defaultHost = "127.0.0.1";
	private static final int defaultPort = 5217;
	private Logger logger = Logger.getLogger("FileClient");
    private String fexusHost;
    private int fexusPort; 
    DependencyRepo repo = new DependencyRepo();
	Socket socket;
    DataInputStream din;
    DataOutputStream dout;
    
	public FaxusClient() throws UnknownHostException, IOException {
		this(defaultHost, defaultPort);
	}
    
	public FaxusClient(String host, int port) throws UnknownHostException, IOException {
		this.fexusHost = host;
		this.fexusPort = port;
		this.socket = new Socket(fexusHost,fexusPort);
		this.din = new  DataInputStream(socket.getInputStream());
		this.dout = new DataOutputStream(socket.getOutputStream());
	}
	
	void uploadDependency(PreCompiledDependency dependency, File depFile) throws IOException, NoSuchDependencyException
	{
		try{
			JSONObject jsonObject = dependency.toRequest();
		
			if (!depFile.exists()) {
				throw new IOException("File "+depFile.getAbsolutePath()+" does not exist");
			}
			dout.writeUTF("PUT");
			dout.writeUTF(jsonObject.toString());
			String response =din.readUTF(); 
			if (response.matches("READY")){
				FileInputStream fin=new FileInputStream(depFile);
		        int ch;
		        do
		        {
		            ch=fin.read();
		            dout.writeUTF(String.valueOf(ch));
		        }
		        while(ch!=-1);
		        String response2 = din.readUTF();
		        if (response.matches("ERROR:.*")) {
		        	logger.log(Level.SEVERE, response);
		        }else if (response.matches("SUCCESS")) {		        	
		        	logger.info("Dependency "+dependency.toString()+" uploaded successfuly");
		        }
			}else if (response.matches("ERROR:.*")){
				logger.log(Level.SEVERE, response);
			}
		} finally {
			dout.writeUTF("DISCONNECT");
		}
	}
	
    public void requestDependency(PreCompiledDependency dependency) throws IOException, NoSuchDependencyException 
    {
    	try {
    		
	    	JSONObject request = dependency.toRequest();
	    	dout.writeUTF("GETDEP");
	        dout.writeUTF(request.toString());
	        String msgFromServer=din.readUTF();
	        
	        if(msgFromServer.matches("ERROR:.*"))
	        {
	        	logger.log(Level.SEVERE, msgFromServer);
	            return;
	        }
	        else if(msgFromServer.matches("READY"))
	        {
	        	logger.info("Fetching "+dependency.toString()+" from fexus@"+fexusHost+":"+fexusPort);
	            File f = repo.getPath(dependency);
	            if (!f.getParentFile().exists()) {
	            	Files.createDirectories(Paths.get(repo.getPath(dependency).getParent()));
	            }
	            if(f.exists())
	            {
	                 logger.log(Level.INFO,"Dependency "+dependency.toString()+ " already downloaded, skipping fetch");             
	            } else {
	            	try (FileOutputStream fout=new FileOutputStream(f)) {            		
	            		int ch;
	            		String temp;
	            		do
	            		{
	            			temp=din.readUTF();
	            			ch=Integer.parseInt(temp);
	            			if(ch!=-1)
	            			{
	            				fout.write(ch);                    
	            			}
	            		}while(ch!=-1);
	            	}
	            }
	            
	            logger.info(din.readUTF());
	                
	        }
    	}finally{
    		dout.writeUTF("DISCONNECT");
    	}
        
        
    }

	public static void main(String[] args) {
		String json = "{\n"
				+ " class: 'CPPStaticLib',\n"
				+ "	arch: '"+System.getProperty("os.arch")+"',\n"
				+ "	compiler: 'gcc',\n"
				+ "	system:' "+System.getProperty("os.name")+"',\n"
				+ "	name: 'baash',\n"
				+ "	version: '2.2.1'\n"
				+ " }";
//		String json = "{\n"
//				+ " class: 'CPPStaticLib',\n"
//				+ "	arch: '"+"WINDOWS"+"',\n"
//				+ "	compiler: 'gcc',\n"
//				+ "	system:' "+CppArtifactSystem.getProperty("os.name")+"',\n"
//				+ "	name: 'somelib',\n"
//				+ "	version: '5'\n"
//				+ " }";
		CPPStaticLib lib = new CPPStaticLib(new JSONObject(json));
		
		FaxusClient client;
		
		try {
			System.out.println("Request: "+json);
			client = new FaxusClient();
			client.requestDependency(lib);
//			client.uploadDependency(lib, new File("/home/mexorsu/.bashrc"));
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchDependencyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	
}
