package edu.szyrek.myveds;

import java.io.File;
import java.util.logging.Logger;

import edu.szyrek.faxus.filesystem.DependencyRepo;
import edu.szyrek.faxus.server.FileServer;


public class Main {
	
	public static String rootDir = System.getProperty("user.home")+File.separator+".faxus";
	private static final Logger logger = Logger.getLogger("FileServer");
	
	public static void main(String[] args) throws Exception {

    	FileServer server = new FileServer();
    	server.configureFromFile(rootDir+File.separator+"config.prop");
    	server.start();
    }
}
