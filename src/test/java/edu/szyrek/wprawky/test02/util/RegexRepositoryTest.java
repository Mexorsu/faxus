package edu.szyrek.wprawky.test02.util;

import edu.szyrek.wprawky.test02.model.cpp.CppArtifactSystem;
import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Pattern;

import static edu.szyrek.wprawky.test02.util.RegexRepository.*;
/**
 * Created by wishm on 28/11/2016.
 */
public class RegexRepositoryTest {

    private static final String linuxstaticLib1 = "libopengl.a";
    private static final String linuxstaticLib2 = "libopen_cv.a";
    private static final String linuxstaticLib3 = "libxerces.a.1.2.3";

    private static final Pattern LINUX_STATIC_LIB_PATTERN = STATIC_LIB_PATTERN(CppArtifactSystem.LINUX);

    private static final String linuxdynamicLib1 = "libopengl.so";
    private static final String linuxdynamicLib2 = "libopen_cv.so";
    private static final String linuxdynamicLib3 = "libxerces.so.1.2.3";

    private static final Pattern LINUX_DYNAMIC_LIB_PATTERN = DYNAMIC_LIB_PATTERN(CppArtifactSystem.LINUX);

    private static final String windowsstaticLib1 = "opengl.lib";
    private static final String windowsstaticLib2 = "open_cv.lib";
    private static final String windowsstaticLib3 = "xerces.lib";

    private static final Pattern WINDOWS_STATIC_LIB_PATTERN = STATIC_LIB_PATTERN(CppArtifactSystem.WINDOWS);

    private static final String windowsdynamicLib1 = "opengl.dll";
    private static final String windowsdynamicLib2 = "open_cv.dll";
    private static final String windowsdynamicLib3 = "xerces.dll";

    private static final Pattern WINDOWS_DYNAMIC_LIB_PATTERN = DYNAMIC_LIB_PATTERN(CppArtifactSystem.WINDOWS);

    private static final String linuxexecutable1 = "libopengl";
    private static final String linuxexecutable2 = "libopen_cv";
    private static final String linuxexecutable3 = "libxerces";

    private static final Pattern LINUX_EXECUTABLE_PATTERN = EXECUTABLE_PATTERN(CppArtifactSystem.LINUX);

    private static final String windowsexecutable1 = "opengl.exe";
    private static final String windowsexecutable2 = "open_cv.exe";
    private static final String windowsexecutable3 = "xerces.exe";

    private static final Pattern WINDOWS_EXECUTABLE_PATTERN = EXECUTABLE_PATTERN(CppArtifactSystem.WINDOWS);




    @Test
    public void testWindowsStaticLibRegex() {
        Assert.assertTrue(patternMatches(WINDOWS_STATIC_LIB_PATTERN,  windowsstaticLib1));
        Assert.assertTrue(patternMatches(WINDOWS_STATIC_LIB_PATTERN,  windowsstaticLib2));
        Assert.assertTrue(patternMatches(WINDOWS_STATIC_LIB_PATTERN,  windowsstaticLib3));

        Assert.assertFalse(patternMatches(WINDOWS_STATIC_LIB_PATTERN,  windowsdynamicLib1));
        Assert.assertFalse(patternMatches(WINDOWS_STATIC_LIB_PATTERN,  windowsdynamicLib2));
        Assert.assertFalse(patternMatches(WINDOWS_STATIC_LIB_PATTERN,  windowsdynamicLib3));
    }

    @Test
    public void testWindowsDynamicLibRegex() {
        Assert.assertTrue(patternMatches(WINDOWS_DYNAMIC_LIB_PATTERN,  windowsdynamicLib1));
        Assert.assertTrue(patternMatches(WINDOWS_DYNAMIC_LIB_PATTERN,  windowsdynamicLib2));
        Assert.assertTrue(patternMatches(WINDOWS_DYNAMIC_LIB_PATTERN,  windowsdynamicLib3));

        Assert.assertFalse(patternMatches(WINDOWS_DYNAMIC_LIB_PATTERN,  windowsstaticLib1));
        Assert.assertFalse(patternMatches(WINDOWS_DYNAMIC_LIB_PATTERN,  windowsstaticLib2));
        Assert.assertFalse(patternMatches(WINDOWS_DYNAMIC_LIB_PATTERN,  windowsstaticLib3));
    }

    @Test
    public void testLinuxStaticLibRegex() {
        Assert.assertTrue(patternMatches(LINUX_STATIC_LIB_PATTERN,  linuxstaticLib1));
        Assert.assertTrue(patternMatches(LINUX_STATIC_LIB_PATTERN,  linuxstaticLib2));
        Assert.assertTrue(patternMatches(LINUX_STATIC_LIB_PATTERN,  linuxstaticLib3));

        Assert.assertFalse(patternMatches(LINUX_STATIC_LIB_PATTERN,  linuxdynamicLib1));
        Assert.assertFalse(patternMatches(LINUX_STATIC_LIB_PATTERN,  linuxdynamicLib2));
        Assert.assertFalse(patternMatches(LINUX_STATIC_LIB_PATTERN,  linuxdynamicLib3));
    }

    @Test
    public void testLinuxDynamicLibRegex() {
        Assert.assertTrue(patternMatches(LINUX_DYNAMIC_LIB_PATTERN,  linuxdynamicLib1));
        Assert.assertTrue(patternMatches(LINUX_DYNAMIC_LIB_PATTERN,  linuxdynamicLib2));
        Assert.assertTrue(patternMatches(LINUX_DYNAMIC_LIB_PATTERN,  linuxdynamicLib3));

        Assert.assertFalse(patternMatches(LINUX_DYNAMIC_LIB_PATTERN,  linuxstaticLib1));
        Assert.assertFalse(patternMatches(LINUX_DYNAMIC_LIB_PATTERN,  linuxstaticLib2));
        Assert.assertFalse(patternMatches(LINUX_DYNAMIC_LIB_PATTERN,  linuxstaticLib3));
    }

    @Test
    public void testLinuxExecutableRegex() {
        Assert.assertTrue(patternMatches(LINUX_EXECUTABLE_PATTERN,  linuxexecutable1));
        Assert.assertTrue(patternMatches(LINUX_EXECUTABLE_PATTERN,  linuxexecutable2));
        Assert.assertTrue(patternMatches(LINUX_EXECUTABLE_PATTERN,  linuxexecutable3));

        Assert.assertFalse(patternMatches(LINUX_EXECUTABLE_PATTERN,  windowsexecutable1));
        Assert.assertFalse(patternMatches(LINUX_EXECUTABLE_PATTERN,  windowsexecutable2));
        Assert.assertFalse(patternMatches(LINUX_EXECUTABLE_PATTERN,  windowsexecutable3));
    }

    @Test
    public void testWindowsExecutableRegex() {
        Assert.assertTrue(patternMatches(WINDOWS_EXECUTABLE_PATTERN,  windowsexecutable1));
        Assert.assertTrue(patternMatches(WINDOWS_EXECUTABLE_PATTERN,  windowsexecutable2));
        Assert.assertTrue(patternMatches(WINDOWS_EXECUTABLE_PATTERN,  windowsexecutable3));

        Assert.assertFalse(patternMatches(WINDOWS_EXECUTABLE_PATTERN,  linuxexecutable1));
        Assert.assertFalse(patternMatches(WINDOWS_EXECUTABLE_PATTERN,  linuxexecutable2));
        Assert.assertFalse(patternMatches(WINDOWS_EXECUTABLE_PATTERN,  linuxexecutable3));
    }
    
}
