package edu.szyrek.wprawky.test02.model;

import edu.szyrek.wprawky.test02.model.cpp.CppArtifactSystem;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wishm on 30/11/2016.
 */
public class SystemTest {
    

private final static String win1 = "Windows XP";
private final static String win2 = "Windows 2003";
private final static String linux = "Linux";
private final static String win3 = "Windows 2000";
private final static String win4 = "Windows 2000";
private final static String osx = "Mac OS X";
private final static String win5 = "Windows 98";
private final static String sunos = "SunOS";
private final static String freebds = "FreeBSD";
private final static String win6 = "Windows NT";
private final static String win7 = "Windows Me";
    
    

    @Test
    public void testWindows() {
        Assert.assertEquals(CppArtifactSystem.fromString(win1), CppArtifactSystem.WINDOWS);
        Assert.assertEquals(CppArtifactSystem.fromString(win2), CppArtifactSystem.WINDOWS);
        Assert.assertEquals(CppArtifactSystem.fromString(win3), CppArtifactSystem.WINDOWS);
        Assert.assertEquals(CppArtifactSystem.fromString(win4), CppArtifactSystem.WINDOWS);
        Assert.assertEquals(CppArtifactSystem.fromString(win5), CppArtifactSystem.WINDOWS);
        Assert.assertEquals(CppArtifactSystem.fromString(win6), CppArtifactSystem.WINDOWS);
        Assert.assertEquals(CppArtifactSystem.fromString(win7), CppArtifactSystem.WINDOWS);
    }

    @Test
    public void testOSX() {
        Assert.assertEquals(CppArtifactSystem.fromString(osx), CppArtifactSystem.OSX);
    }


    @Test
    public void testBSD() {
        Assert.assertEquals(CppArtifactSystem.fromString(freebds), CppArtifactSystem.FREEBSD);
    }

    @Test
    public void testSolaris() {
        Assert.assertEquals(CppArtifactSystem.fromString(sunos), CppArtifactSystem.SOLARIS);
    }


    @Test
    public void testLinux() {
        Assert.assertEquals(CppArtifactSystem.fromString(linux), CppArtifactSystem.LINUX);
    }

}
