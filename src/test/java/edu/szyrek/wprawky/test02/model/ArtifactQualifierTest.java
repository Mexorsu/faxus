package edu.szyrek.wprawky.test02.model;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by wishm on 28/11/2016.
 */
public class ArtifactQualifierTest {

    private static class TestQualifier extends ArtifactQualifier {
        private int testValue;

        public TestQualifier(int val) {
            this.testValue = val;
        }

        @Override
        public boolean qualifies(ArtifactQualifier value) {
            return this.testValue == ((TestQualifier)value).testValue;
        }

        @Override
        public int compareTo(Object o) {
            return Integer.compare(this.testValue, ((TestQualifier)o).testValue);
        }
    }

    private static TestQualifier test1;
    private static TestQualifier test2;
    private static TestQualifier test3;

    @BeforeClass
    public static void setup() {
        test1 = new TestQualifier(1);
        test2 = new TestQualifier(2);
        test3 = new TestQualifier(1);
    }

    @Test
    public void testComparator() {
        Assert.assertTrue(test1.compareTo(test2)<0);
        Assert.assertTrue(test2.compareTo(test1)>0);
        Assert.assertEquals(test1.compareTo(test3), 0);
        Assert.assertEquals(test3.compareTo(test1), 0);
    }

    @Test
    public void testQualifies() {
        Assert.assertTrue(test1.qualifies(test3));
        Assert.assertTrue(test3.qualifies(test1));
        Assert.assertFalse(test1.qualifies(test2));
        Assert.assertFalse(test2.qualifies(test3));
    }

}
