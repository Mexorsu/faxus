package edu.szyrek.wprawky.test02.model;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by wishm on 26/11/2016.
 */
public class VersionTest {
    // GIVEN
    public static Version reference;
    public static Version older;
    public static Version newer;

    public static Version newerThanOrEqualTo_1;
    public static Version newerThanOrEqualTo_1_1;

    public static Version fromString1;
    public static Version fromString2;
    public static Version fromString3;
    public static Version fromString4;

    public static Version fromString5;
    public static Version fromString6;
    public static Version fromString7;
    public static Version fromString8;

    // WHEN
    @BeforeClass
    public static void setup() {
        reference = new Version(1,0,0);
        older = new Version(0,0,9);
        newer = new Version(1,1,1);
        newerThanOrEqualTo_1 = new Version(1, null, null);
        newerThanOrEqualTo_1_1 = new Version(1, 1, null);

        fromString1 = new Version("");
        fromString2 = new Version("X");
        fromString3 = new Version("x");
        fromString4 = new Version("x.X");

        fromString5 = new Version("1.x.x");
        fromString6 = new Version("X.1.x");
        fromString7 = new Version("x.x.1");
        fromString8 = new Version("x.X.x");

    }

    @Test
    public void testFromString() {
        Assert.assertEquals(Version.ANY, fromString1);
        Assert.assertEquals(Version.ANY, fromString2);
        Assert.assertEquals(Version.ANY, fromString3);
        Assert.assertEquals(Version.ANY, fromString4);

        Assert.assertEquals(new Version(1, null, null), fromString5);
        Assert.assertEquals(new Version(null, 1, null), fromString6);
        Assert.assertEquals(new Version(null, null, 1), fromString7);
        Assert.assertEquals(Version.ANY, fromString8);
    }

    // THEN
    @Test
    public void testQualifies() {
        Assert.assertTrue(reference.qualifies(newerThanOrEqualTo_1));
        Assert.assertTrue(newer.qualifies(newerThanOrEqualTo_1));
        Assert.assertFalse(older.qualifies(newerThanOrEqualTo_1));
        Assert.assertTrue(newerThanOrEqualTo_1_1.qualifies(newerThanOrEqualTo_1));
        Assert.assertFalse(newerThanOrEqualTo_1.qualifies(newerThanOrEqualTo_1_1));
        Assert.assertTrue(reference.qualifies(reference));
        Assert.assertTrue(older.qualifies(older));
        Assert.assertTrue(newer.qualifies(newer));
        Assert.assertTrue(newerThanOrEqualTo_1.qualifies(newerThanOrEqualTo_1));
        Assert.assertTrue(newerThanOrEqualTo_1_1.qualifies(newerThanOrEqualTo_1_1));
        Assert.assertFalse(reference.qualifies(newerThanOrEqualTo_1_1));
        Assert.assertTrue(newer.qualifies(newerThanOrEqualTo_1_1));
        Assert.assertFalse(older.qualifies(newerThanOrEqualTo_1));
    }

    @Test
    public void testIsOlderThan() {
        Assert.assertTrue(older.isOlderThan(reference));
        // check also reverse relation
        Assert.assertFalse(reference.isOlderThan(older));
        // assert transitiveness
        Assert.assertTrue(older.isOlderThan(newer));
        Assert.assertFalse(newer.isOlderThan(older));
    }

    @Test
    public void testIsNewerThan() {
        Assert.assertTrue(newer.isNewerThan(reference));
        // check also reverse relation
        Assert.assertFalse(reference.isNewerThan(newer));
        // assert transitiveness
        Assert.assertTrue(newer.isNewerThan(older));
        Assert.assertFalse(older.isNewerThan(newer));
    }

    @Test
    public void testLatest() {
        // Older/newer
        Assert.assertTrue(Version.LATEST.isNewerThan(newer));
        Assert.assertTrue(Version.LATEST.isNewerThan(older));
        Assert.assertTrue(Version.LATEST.isNewerThan(reference));
        // Abstract examples
        Assert.assertTrue(Version.LATEST.qualifies(Version.LATEST));
        Assert.assertTrue(Version.LATEST.qualifies(Version.ANY));

        // Concrete examples
        Assert.assertFalse(Version.LATEST.qualifies(newerThanOrEqualTo_1_1));
        Assert.assertFalse(Version.LATEST.qualifies(newerThanOrEqualTo_1));
    }

    @Test
    public void testAny() {
        // Older/newer
        Assert.assertTrue(Version.ANY.isOlderThan(newer));
        Assert.assertTrue(Version.ANY.isOlderThan(older));
        Assert.assertTrue(Version.ANY.isOlderThan(reference));

        // Abstract
        Assert.assertTrue(Version.ANY.qualifies(Version.ANY));
        Assert.assertFalse(Version.ANY.qualifies(Version.LATEST));

        // Concrete
        Assert.assertTrue(reference.qualifies(Version.ANY));
        Assert.assertTrue(older.qualifies(Version.ANY));
        Assert.assertTrue(newer.qualifies(Version.ANY));
        Assert.assertTrue(newerThanOrEqualTo_1.qualifies(Version.ANY));
        Assert.assertTrue(newerThanOrEqualTo_1_1.qualifies(Version.ANY));
        Assert.assertFalse(Version.ANY.qualifies(newerThanOrEqualTo_1));
        Assert.assertFalse(Version.ANY.qualifies(newerThanOrEqualTo_1_1));
    }


}
