package edu.szyrek.wprawky.test02.model.java;

import edu.szyrek.wprawky.test02.model.ArtifactId;
import org.junit.Assert;
import org.junit.Test;
import java.io.File;

/**
 * Created by wishm on 28/11/2016.
 */
public class JavaArtifactIdTest {
    private static final String testGroupId = "edu.szyrek.wprawky";
    private static final String testArtifactId = "test";
    private static final String differentGroupId = "edu.szyrek";
    private static final String differentArtifactId = "test2";
    private final static String testString = testGroupId+":"+testArtifactId;
    private final static ArtifactId reference = new JavaArtifactId(testGroupId, testArtifactId);
    private final static String[] referencePath = new String[]{"edu", "szyrek", "wprawky", "test"};
    private final static ArtifactId different = new JavaArtifactId(differentGroupId, differentArtifactId);
    private final static ArtifactId fromString = new JavaArtifactId(testString);
    private final static String toString = reference.toString();

    private String getFileSeparatorRegex() {
        if (File.separator.equals("\\")) {
            return "\\\\";
        } else {
            return "/";
        }
    }

    @Test
    public void testJSONCreator() {
        Assert.assertEquals(reference, fromString);
    }

    @Test
    public void testToString() {
        Assert.assertEquals(testString, toString);
    }

    @Test
    public void testQualifies() {
        Assert.assertTrue(reference.qualifies(fromString));
        Assert.assertTrue(fromString.qualifies(reference));

        Assert.assertFalse(reference.qualifies(different));
        Assert.assertFalse(different.qualifies(reference));

        Assert.assertFalse(fromString.qualifies(different));
        Assert.assertFalse(different.qualifies(fromString));
    }

    @Test
    public void testToPath() {
        Assert.assertArrayEquals(referencePath, reference.toPath().split(getFileSeparatorRegex()));
    }

    @Test
    public void testGetName() {
        Assert.assertEquals(testArtifactId, reference.getName());
    }

}
