package edu.szyrek.wprawky.test02.model;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by wishm on 28/11/2016.
 */
public class ArchitectureTest {

    private static Architecture testi686;
    private static Architecture testx64;
    private static Architecture testPowerPC;

    @BeforeClass
    public static void setup() {
        testi686 = Architecture.fromString("i686");
        testx64 = Architecture.fromString("amd64");
        testPowerPC = Architecture.valueOf("POWER_PC");
    }

    @Test
    public void testValueOf() {
        Assert.assertEquals(Architecture.POWER_PC, testPowerPC);
    }

    @Test
    public void testFromString() {
        Assert.assertEquals(Architecture.X86, testi686);
        Assert.assertEquals(Architecture.X64, testx64);
    }

}
