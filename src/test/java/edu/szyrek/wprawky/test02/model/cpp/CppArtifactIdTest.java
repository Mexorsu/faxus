package edu.szyrek.wprawky.test02.model.cpp;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * Created by wishm on 28/11/2016.
 */
public class CppArtifactIdTest {
    private final static String testName = "opengl";
    private final static String differentName = "opengl2";
    private final static String toString = "opengl";

    private final static CppArtifactId reference = new CppArtifactId(testName);
    private final static CppArtifactId different = new CppArtifactId(differentName);
    private final static CppArtifactId same = new CppArtifactId(testName);

    private final static String[] referencePath = new String[]{"opengl"};

    private String getFileSeparatorRegex() {
        if (File.separator.equals("\\")) {
            return "\\\\";
        } else {
            return "/";
        }
    }


    @Test
    public void testToString() {
        Assert.assertEquals(toString, reference.toString());
    }

    @Test
    public void testToPath() {
        Assert.assertArrayEquals(referencePath, reference.toPath().split(getFileSeparatorRegex()));
    }

    @Test
    public void testQualifies() {
        Assert.assertTrue(reference.qualifies(same));
        Assert.assertTrue(same.qualifies(reference));
        Assert.assertFalse(reference.qualifies(different));
        Assert.assertFalse(different.qualifies(reference));
        Assert.assertFalse(same.qualifies(different));
        Assert.assertFalse(different.qualifies(same));
    }
}
