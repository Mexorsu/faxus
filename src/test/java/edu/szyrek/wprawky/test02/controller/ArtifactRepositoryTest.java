package edu.szyrek.wprawky.test02.controller;

import edu.szyrek.wprawky.test02.model.ArtifactDescriptor;
import edu.szyrek.wprawky.test02.model.Version;
import edu.szyrek.wprawky.test02.model.java.JavaArtifactId;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by wishm on 26/11/2016.
 */
public class ArtifactRepositoryTest {

    public static final String REFERENCE_GROUPID = "edu.szyrek";
    public static final String REFERENCE_ARTIFACTID_1 = "test-artifact1";
    public static final String REFERENCE_ARTIFACTID_2 = "test-artifact2";
    public static final String REFERENCE_ARTIFACTID_3 = "test-artifact3";

    public static ArtifactDescriptor testDescriptor1;
    public static ArtifactDescriptor latest1;
    public static ArtifactDescriptor testDescriptor2;
    public static ArtifactDescriptor latest2;
    public static ArtifactDescriptor testDescriptor3;
    public static ArtifactDescriptor latest3;
    public static DefaultArtifactRepository repository;

    public static ArtifactDescriptor test1 = new ArtifactDescriptor();
    public static ArtifactDescriptor test2 = new ArtifactDescriptor();
    public static ArtifactDescriptor test3 = new ArtifactDescriptor();
    public static ArtifactDescriptor test4 = new ArtifactDescriptor();
    public static ArtifactDescriptor test5 = new ArtifactDescriptor();
    public static ArtifactDescriptor test6 = new ArtifactDescriptor();

    public static ArtifactDescriptor all10 = new ArtifactDescriptor();
    public static ArtifactDescriptor all2 = new ArtifactDescriptor();

    @BeforeClass
    public static void setup() {
        repository = new DefaultArtifactRepository();

        testDescriptor1  = new ArtifactDescriptor();
        testDescriptor1.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_1));
        latest1  = new ArtifactDescriptor();
        latest1.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_1));
        latest1.addQualifier(Version.LATEST);
        testDescriptor2  = new ArtifactDescriptor();
        testDescriptor2.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_2));
        latest2  = new ArtifactDescriptor();
        latest2.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_2));
        latest2.addQualifier(Version.LATEST);
        testDescriptor3  = new ArtifactDescriptor();
        testDescriptor3.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_3));
        latest3  = new ArtifactDescriptor();
        latest3.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_3));
        latest3.addQualifier(Version.LATEST);


        test1.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_1));
        test1.addQualifier(new Version(1,0,0));
        repository.add(test1);

        test2.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_1));
        test2.addQualifier(new Version(1,2,0));
        repository.add(test2);

        test3.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_2));
        test3.addQualifier(new Version(1,0,0));
        repository.add(test3);

        test4.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_2));
        test4.addQualifier(new Version(2,0,0));
        repository.add(test4);

        test5.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_3));
        test5.addQualifier(new Version(1,0,0));
        repository.add(test5);

        test6.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_3));
        test6.addQualifier(Version.LATEST);
        repository.add(test6);

        all10.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_1));
        all10.addQualifier(new Version("1.x.0"));

        all2.addQualifier(new JavaArtifactId(REFERENCE_GROUPID, REFERENCE_ARTIFACTID_2));
        all2.addQualifier(new Version("2.x.x"));

    }

    @Test
    public void testVersionFilter() {
        Assert.assertEquals(2, repository.filter(all10).size());
        Assert.assertEquals(1, repository.filter(all2).size());
    }

    @Test
    public void testGet() {
        Assert.assertEquals(repository.get(latest1), test2);
        Assert.assertEquals(repository.get(latest2), test4);
        Assert.assertEquals(repository.get(latest3), test6);
    }


    @Test
    public void testGetLatest() {
        Assert.assertEquals(repository.getLatest(testDescriptor1), test2);
        Assert.assertEquals(repository.getLatest(testDescriptor2), test4);
        Assert.assertEquals(repository.getLatest(testDescriptor3), test6);
    }

    @Test
    public void testGetLatestByDescriptor() {
        Assert.assertEquals(repository.filter(latest1).get(0), test2);
        Assert.assertEquals(repository.filter(latest1).size(),2);
        Assert.assertEquals(repository.filter(latest2).get(0), test4);
        Assert.assertEquals(repository.filter(latest2).size(),2);
        Assert.assertEquals(repository.filter(latest3).get(0), test6);
        Assert.assertEquals(repository.filter(latest3).size(),2);
    }

    @Test
    public void testGetSpecific() {
        Assert.assertTrue(repository.filter(test1).contains(test1));
        Assert.assertTrue(repository.filter(test2).contains(test2));
        Assert.assertTrue(repository.filter(test3).contains(test3));
        Assert.assertTrue(repository.filter(test4).contains(test4));
        Assert.assertTrue(repository.filter(test5).contains(test5));
        Assert.assertTrue(repository.filter(test6).contains(test6));


    }


}
