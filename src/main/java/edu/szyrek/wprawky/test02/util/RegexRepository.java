package edu.szyrek.wprawky.test02.util;

import edu.szyrek.wprawky.test02.model.cpp.CppArtifactSystem;

import java.util.regex.Pattern;

/**
 * Created by wishm on 28/11/2016.
 */
public class RegexRepository {
    private final static String FILE_NAME_ALLOWED_CHARS = "a-zA-Z0-9_-";
    public final static Pattern WEB_XML_PATTERN = Pattern.compile("web\\.xml");
    public final static Pattern POM_XML_PATTERN = Pattern.compile("pom\\.xml");
    public final static Pattern XML_FILE_PATTERN = Pattern.compile(FILE_NAME_ALLOWED_CHARS+"\\.xml");
    public final static Pattern EAR_FILE_PATTERN = Pattern.compile(FILE_NAME_ALLOWED_CHARS+"\\.ear");
    public final static Pattern WAR_FILE_PATTERN = Pattern.compile(FILE_NAME_ALLOWED_CHARS+"\\.war");
    public final static Pattern JAR_FILE_PATTERN = Pattern.compile(FILE_NAME_ALLOWED_CHARS+"\\.jar");


    public static final Pattern WINDOWS_PATTERN = Pattern.compile(".*win(dows)?.*");
    public static final Pattern LINUX_PATTERN = Pattern.compile(".*(linux|gnu).*");
    public static final Pattern OSX_PATTERN = Pattern.compile(".*(mac[ \t]*os[ \t]*(x)?|os[ \t]*x).*");
    public static final Pattern SOLARIS_PATTERN = Pattern.compile(".*(sun[ \t]*os|solaris).*");
    public static final Pattern FREEBSD_PATTERN = Pattern.compile(".*(free[ \t]*bsd).*");


    public static Pattern DYNAMIC_LIB_PATTERN(final CppArtifactSystem system) {
        return Pattern.compile(
                "("+system.getDynamicLibPrefix()+")?" +
                        "(["+ FILE_NAME_ALLOWED_CHARS +"]+)\\." +
                        system.getDynamicLibExtension() +
                        "(\\.[0-9]+)*"
        );
    }

    public static Pattern DYNAMIC_LIB_PATTERN() {
        final CppArtifactSystem system = CppArtifactSystem.fromString(System.getProperty("os.name"));
        return DYNAMIC_LIB_PATTERN(system);
    }

    public static Pattern STATIC_LIB_PATTERN(final CppArtifactSystem system) {
            return Pattern.compile(
                    "("+system.getStaticLibPrefix()+")?" +
                    "(["+ FILE_NAME_ALLOWED_CHARS +"]+)\\." +
                    system.getStaticLibExtension() +
                    "(\\.[0-9]+)*"
            );
    }

    public static Pattern STATIC_LIB_PATTERN() {
        final CppArtifactSystem system = CppArtifactSystem.fromString(System.getProperty("os.name"));
        return STATIC_LIB_PATTERN(system);
    }


    public static Pattern EXECUTABLE_PATTERN(final CppArtifactSystem system) {
        if (system.equals(CppArtifactSystem.WINDOWS)) {
            return Pattern.compile("["+FILE_NAME_ALLOWED_CHARS+"]+.exe");
        } else {
            return Pattern.compile("["+FILE_NAME_ALLOWED_CHARS+"]+");
        }
    }

    public static Pattern EXECUTABLE_PATTERN() {
        final CppArtifactSystem system = CppArtifactSystem.fromString(System.getProperty("os.name"));
        return STATIC_LIB_PATTERN(system);
    }

    public static boolean patternMatches(final Pattern pattern, final String input) {
        return pattern.matcher(input).matches();
    }

}
