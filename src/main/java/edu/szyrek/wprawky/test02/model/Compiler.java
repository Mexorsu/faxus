package edu.szyrek.wprawky.test02.model;

public enum Compiler {
	GCC, MSVC, CLANG, BORLAND, MINGW, NOT_SUPPORTED;
	
	public static Compiler fromString(String string) {
		for (Compiler compiler: Compiler.values()) {
			if (string.toUpperCase().equals(compiler.toString())) {
				return compiler;
			}
		}
		return NOT_SUPPORTED;
	}
}
