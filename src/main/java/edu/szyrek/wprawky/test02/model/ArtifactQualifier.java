package edu.szyrek.wprawky.test02.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import edu.szyrek.wprawky.test02.model.cpp.CppArtifactId;
import edu.szyrek.wprawky.test02.model.cpp.CppArtifactType;
import edu.szyrek.wprawky.test02.model.cpp.CppVersion;
import edu.szyrek.wprawky.test02.model.java.JavaArtifactId;
import edu.szyrek.wprawky.test02.model.java.JavaArtifactType;
import edu.szyrek.wprawky.test02.model.java.JavaVersion;
import lombok.EqualsAndHashCode;

import java.util.Comparator;

/**
 * Created by wishm on 26/11/2016.
 */

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "id", value = ArtifactId.class),
        @JsonSubTypes.Type(name = "java", value = JavaArtifactId.class),
        @JsonSubTypes.Type(name = "cpp", value = CppArtifactId.class),
        @JsonSubTypes.Type(name = "ver", value = Version.class),
        @JsonSubTypes.Type(name = "qualifier", value = JavaQualifier.class),
        @JsonSubTypes.Type(name = "cppver", value = CppVersion.class),
        @JsonSubTypes.Type(name = "jver", value = JavaVersion.class),
        @JsonSubTypes.Type(name = "target", value = CppArtifactType.class),
        @JsonSubTypes.Type(name = "type", value = ArtifactType.class),
        @JsonSubTypes.Type(name = "packaging", value = JavaArtifactType.class),
        @JsonSubTypes.Type(name = "resolver", value = ArtifactResolver.class)})
public abstract class ArtifactQualifier implements Comparable {
    public abstract boolean qualifies(ArtifactQualifier value);
    public static Comparator<ArtifactQualifier> comparator = (o1, o2) -> o1.compareTo(o2);
}
