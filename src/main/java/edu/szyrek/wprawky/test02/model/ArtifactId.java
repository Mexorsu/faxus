package edu.szyrek.wprawky.test02.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import edu.szyrek.wprawky.test02.model.cpp.CppArtifactId;
import edu.szyrek.wprawky.test02.model.cpp.CppVersion;
import edu.szyrek.wprawky.test02.model.java.JavaArtifactId;
import edu.szyrek.wprawky.test02.model.java.JavaVersion;
import lombok.EqualsAndHashCode;

import java.util.Objects;

/**
 * Created by wishm on 26/11/2016.
 */

@EqualsAndHashCode(callSuper = false)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
@JsonSubTypes({ @JsonSubTypes.Type(name = "java", value = JavaArtifactId.class),@JsonSubTypes.Type(name = "cpp", value = CppArtifactId.class) })
public abstract class ArtifactId extends ArtifactQualifier {
    public abstract String toPath();
    public abstract String getName();

    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    public boolean qualifies(ArtifactQualifier value) {
        return this.equals(value);
    }

    @Override
    public int compareTo(Object o) {
        if (this.equals(o))
            return 0;
        else {
            JavaArtifactId other = (JavaArtifactId) o;
            return Objects.compare(this.toString(), other.toString(), String.CASE_INSENSITIVE_ORDER);
        }
    }
}
