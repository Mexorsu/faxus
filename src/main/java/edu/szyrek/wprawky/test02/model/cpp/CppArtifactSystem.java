package edu.szyrek.wprawky.test02.model.cpp;

import static edu.szyrek.wprawky.test02.util.RegexRepository.*;

public enum CppArtifactSystem {
	WINDOWS, LINUX, OSX, SOLARIS, FREEBSD, NOT_SUPPORTED;
	
	public static CppArtifactSystem fromString(final String text) {
	    if (text == null)
	    {
			return null;
		}
		else if (patternMatches(WINDOWS_PATTERN, text.toLowerCase()))
		{
			return WINDOWS;
		}
		else if (patternMatches(LINUX_PATTERN, text.toLowerCase()))
		{
			return LINUX;
		}
		else if (patternMatches(OSX_PATTERN, text.toLowerCase()))
		{
			return OSX;
		}
		else if (patternMatches(SOLARIS_PATTERN, text.toLowerCase()))
		{
			return SOLARIS;
		}
		else if (patternMatches(FREEBSD_PATTERN, text.toLowerCase()))
		{
	    	return FREEBSD;
	    }
	    else
		{
	    	return NOT_SUPPORTED;
	    }
	  }

	public String getStaticLibPrefix() {
		switch (this){
			case WINDOWS: return "";
			default: return "lib";
		}
	}
	public String getStaticLibExtension() {
		switch (this){
			case WINDOWS: return "lib";
			default: return "a";
		}
	}
	public String getDynamicLibPrefix() {
		switch (this){
			case WINDOWS: return "";
			default: return "lib";
		}
	}
	public String getDynamicLibExtension() {
		switch (this){
			case WINDOWS: return "dll";
			default: return "so";
		}
	}

}
