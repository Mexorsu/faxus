package edu.szyrek.wprawky.test02.model;

import java.util.HashMap;
import java.util.Map;

public enum Architecture {
	A68K("A68K"),
	ARM("ARM"),
	ARM_BE("ARM_BE"),
	ARM_LE("ARM_LE"),
	ALPHA("ALPHA"),
	IA64N("IA64N"), IA64W("IA64W"),
	PTSC("PTSC"),
	MIPS("MIPS"),
	SGI("SGI"),
	PARISC("PARISC"),
	SH4("SH4"),
	IGNITE("IGNITE"),
	SPARC("SPARC"),
	S390("S390"),
	S390X("S390X"),
	V850E("V850E"),
	POWER_PC("POWER_PC"),
	POWER_PC_64("POWER_PC_64"),
	POWER_PC_64_LE("POWER_PC_64_LE"),
	X86("X86"),
	X64("X64"),
	NOT_SUPPORTED("NOT SUPPORTED");


	Architecture(String value) {}
 	private static Map<String, Architecture> archAliasesByArchName;

	public static Architecture fromString(String alias) {
		Architecture result = archAliasesByArchName.get(alias);
		if (result==null) {
			return NOT_SUPPORTED;
		} else {
			return result;
		}
	}

	static {
		archAliasesByArchName = new HashMap<>();
		archAliasesByArchName.put("68k", A68K);
		archAliasesByArchName.put("68K", A68K);
		archAliasesByArchName.put("ARM", ARM);
		archAliasesByArchName.put("arm", ARM);
		archAliasesByArchName.put("ARM_BE", ARM_BE);
		archAliasesByArchName.put("ARM_BE", ARM_BE);
		archAliasesByArchName.put("arm_be", ARM_BE);
		archAliasesByArchName.put("ARM_LE", ARM_LE);
		archAliasesByArchName.put("ARM_LE", ARM_LE);
		archAliasesByArchName.put("arm_le", ARM_LE);
		archAliasesByArchName.put("ALPHA", ALPHA);
		archAliasesByArchName.put("ALPHA", ALPHA);
		archAliasesByArchName.put("alpha", ALPHA);
		archAliasesByArchName.put("IA64N", IA64N);
		archAliasesByArchName.put("IA64N", IA64N);
		archAliasesByArchName.put("ia-64n", IA64N);
		archAliasesByArchName.put("ia-64-n", IA64N);
		archAliasesByArchName.put("IA64W", IA64W);
		archAliasesByArchName.put("IA64W", IA64W);
		archAliasesByArchName.put("ia-64w", IA64W);
		archAliasesByArchName.put("ia-64-w", IA64W);
		archAliasesByArchName.put("Ptsc", PTSC);
		archAliasesByArchName.put("ptsc", PTSC);
		archAliasesByArchName.put("PTSC", PTSC);
		archAliasesByArchName.put("MIPS", MIPS);
		archAliasesByArchName.put("MIPS", MIPS);
		archAliasesByArchName.put("mips", MIPS);
		archAliasesByArchName.put("SGI", SGI);
		archAliasesByArchName.put("sgi", SGI);
		archAliasesByArchName.put("parisc", PARISC);
		archAliasesByArchName.put("PARISC", PARISC);
		archAliasesByArchName.put("Parisc", PARISC);
		archAliasesByArchName.put("PARISC", PARISC);
		archAliasesByArchName.put("sh4", SH4);
		archAliasesByArchName.put("SH4", SH4);
		archAliasesByArchName.put("SH4", SH4);
		archAliasesByArchName.put("sparc", SPARC);
		archAliasesByArchName.put("SPARC", SPARC);
		archAliasesByArchName.put("SPARC", SPARC);
		archAliasesByArchName.put("sparcv9", SPARC);
		archAliasesByArchName.put("SPARCv9", SPARC);
		archAliasesByArchName.put("Sparcv9", SPARC);
		archAliasesByArchName.put("S390", S390);
		archAliasesByArchName.put("S390", S390);
		archAliasesByArchName.put("S390X", S390X);
		archAliasesByArchName.put("S390X", S390X);
		archAliasesByArchName.put("V850E", V850E);
		archAliasesByArchName.put("v850e", V850E);
		archAliasesByArchName.put("IGNITE", IGNITE);
		archAliasesByArchName.put("IGNITE", IGNITE);
		archAliasesByArchName.put("ignite", IGNITE);
		archAliasesByArchName.put("POWER_PC", POWER_PC);
		archAliasesByArchName.put("POWERPC", POWER_PC);
		archAliasesByArchName.put("powerpc", POWER_PC);
		archAliasesByArchName.put("POWER_PC-64", POWER_PC_64);
		archAliasesByArchName.put("POWERPC-64", POWER_PC_64);
		archAliasesByArchName.put("powerpc-64", POWER_PC_64);
		archAliasesByArchName.put("POWER_PC-64-LE", POWER_PC_64_LE);
		archAliasesByArchName.put("POWERPC-64-LE", POWER_PC_64_LE);
		archAliasesByArchName.put("powerpc-64-LE", POWER_PC_64_LE);
		archAliasesByArchName.put("PowerPC64", POWER_PC_64);
		archAliasesByArchName.put("POWERPC64", POWER_PC_64);
		archAliasesByArchName.put("powerpc64", POWER_PC_64);
		archAliasesByArchName.put("POWER_PC_64_LE", POWER_PC_64_LE);
		archAliasesByArchName.put("POWERPC64LE", POWER_PC_64_LE);
		archAliasesByArchName.put("powerpc64LE", POWER_PC_64_LE);
		archAliasesByArchName.put("POWER_PC 64", POWER_PC_64);
		archAliasesByArchName.put("POWERPC 64", POWER_PC_64);
		archAliasesByArchName.put("powerpc 64", POWER_PC_64);
		archAliasesByArchName.put("POWER_PC 64 LE", POWER_PC_64_LE);
		archAliasesByArchName.put("POWERPC 64 LE", POWER_PC_64_LE);
		archAliasesByArchName.put("powerpc 64 LE", POWER_PC_64_LE);
		archAliasesByArchName.put("X86", X86);
		archAliasesByArchName.put("pentium", X86);
		archAliasesByArchName.put("i386", X86);
		archAliasesByArchName.put("i486", X86);
		archAliasesByArchName.put("i586", X86);
		archAliasesByArchName.put("i686", X86);
		archAliasesByArchName.put("ix86", X86);
		archAliasesByArchName.put("iX86", X86);
		archAliasesByArchName.put("X86-64", X64);
		archAliasesByArchName.put("amd64", X64);
		archAliasesByArchName.put("em64t", X64);
		archAliasesByArchName.put("x86_64", X64);
		archAliasesByArchName.put("x64", X64);
		archAliasesByArchName.put("64", X64);
	}	
}
