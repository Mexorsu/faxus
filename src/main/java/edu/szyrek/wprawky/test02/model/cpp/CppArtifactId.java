package edu.szyrek.wprawky.test02.model.cpp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import edu.szyrek.wprawky.test02.model.ArtifactId;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by wishm on 26/11/2016.
 */
@EqualsAndHashCode(callSuper = false)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public class CppArtifactId extends ArtifactId {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private CppArtifactType type;

    @Getter
    @Setter
    private CppArtifactSystem system;

    public CppArtifactId(){}

    @JsonCreator
    public CppArtifactId(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }


    @Override
    public String toPath() {
        return name + java.io.File.separator;
    }
}
