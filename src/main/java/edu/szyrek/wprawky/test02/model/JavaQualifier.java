package edu.szyrek.wprawky.test02.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by wishm on 26/11/2016.
 */
@EqualsAndHashCode(callSuper = false)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public class JavaQualifier extends ArtifactQualifier {
    @Getter
    @Setter
    private String value;


    public JavaQualifier() {}

    @JsonCreator
    public JavaQualifier(String value) {this.value = value;}


    @Override
    @JsonValue
    public String toString()
    {
        return this.value;

    }

    @Override
    public boolean qualifies(ArtifactQualifier value) {
        return this.equals(value);
    }

    @Override
    public int compareTo(Object o) {
        if (this.equals(o)) return 0;
        else return -1;
    }
}
