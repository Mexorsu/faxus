package edu.szyrek.wprawky.test02.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import java.util.Objects;

/**
 * Created by wishm on 26/11/2016.
 */
@Validated
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public class Version extends ArtifactQualifier {
    private final static String WILDCARD = "x";
    public static final String SEPARATOR = ".";
    public static final String SEPARATOR_REGEX = "\\.";

    @Getter
    @Setter
    @JsonIgnore
    private Named name;

    public enum Named {
        LATEST, ANY;

        public static Named fromString(final String input) {
            final String value;
            try {
                return Named.valueOf(input);
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static Version ANY = new Version(Named.ANY){
        @Override
        public boolean qualifies(final ArtifactQualifier o) {
            if (o !=null && o instanceof  Version && ((Version) o).name == Named.ANY) {
                return true;
            } else {
                return false;
            }
        }
    };


    public static Version LATEST = new Version(Named.LATEST){
        @Override
        public boolean qualifies(final ArtifactQualifier o) {
            if (o !=null && o instanceof  Version &&
                    (
                            (((Version) o).name == Named.LATEST) ||
                                    (((Version) o).name == Named.ANY))

                    ){
                return true;
            } else {
                return false;
            }
        }
    };

    @Getter
    @Setter
    @Min(value = 0, message = "major version cannot be lower than 0")
    @JsonIgnore
    private Integer major;
    @Getter
    @Setter
    @Min(value = 0, message = "minor version cannot be lower than 0")
    @JsonIgnore
    private Integer minor;
    @Getter
    @Setter
    @Min(value = 0, message = "micro version cannot be lower than 0")
    @JsonIgnore
    private Integer micro;

    public Version(final Named name) {
        this.name = name;
    }

    public Version() {

    }

    public Version(Integer major, Integer minor, Integer micro) {
        this.major = major;
        this.minor = minor;
        this.micro = micro;
    }

    @JsonCreator
    public Version(String strThing)
    {
        fromString(strThing);
    }

    @Override
    @JsonValue
    public String toString()
    {
        if (this.name!=null) {
            return this.name.toString();
        } else {
            return (this.major == null ? WILDCARD : this.major) + SEPARATOR
                    + (this.minor == null ? WILDCARD : this.minor) + SEPARATOR
                    + (this.micro == null ? WILDCARD : this.micro);
        }
    }

    private boolean hasSmallerMajorThan(final Version o) {
        if (o == null) return false;
        if (this.major!=null && o.major == null) return false;
        if (this.major == null && o.major == null) return false;
        return (this.major == null && o.major != null) || this.major < o.major;
    }
    private boolean hasSmallerMinorThan(final Version o) {
        if (o == null) return false;
        if (this.minor!=null && o.minor == null) return false;
        if (this.minor == null && o.minor == null) return false;
        return (this.minor == null && o.minor != null) || this.minor < o.minor;
    }
    private boolean hasSmallerMicroThan(final Version o) {
        if (o == null) return false;
        if (this.micro!=null && o.micro == null) return false;
        if (this.micro == null && o.micro == null) return false;
        return (this.micro == null && o.micro != null) || this.micro < o.micro;
    }

    private boolean hasBiggerMajorThan(final Version o) {
        if (o == null) return false;
        if (this.major!=null && o.major == null) return false;
        if (this.major == null && o.major == null) return false;
        return (this.major != null && o.major == null) || this.major > o.major;
    }
    private boolean hasBiggerMinorThan(final Version o) {
        if (o == null) return false;
        if (this.minor!=null && o.minor == null) return false;
        if (this.minor == null && o.minor == null) return false;
        return (this.minor != null && o.minor == null) || this.minor > o.minor;
    }
    private boolean hasBiggerMicroThan(final Version o) {
        if (o == null) return false;
        if (this.micro!=null && o.micro == null) return false;
        if (this.micro == null && o.micro == null) return false;
        return (this.micro != null && o.micro == null) || this.micro > o.micro;
    }

    @Override
    public int compareTo(Object o) {
        if (o !=null && o instanceof  Version) {
            Version other = (Version) o;
            if (this.name != null && this.name.equals(other.getName())) {
                return 0;
            } else if (this.name != null && other.name != null) {
                return Integer.compare(this.name.ordinal(), other.name.ordinal());
            } else if (this.name == Named.ANY) {
                return -1;
            } else if (this.name == Named.LATEST) {
                return 1;
            } else if (other.name == Named.ANY) {
                return 1;
            } else if (other.name == Named.LATEST) {
                return -1;
            } else if (hasSmallerMajorThan(other)) {
                return -1;
            } else if (hasBiggerMajorThan(other)) {
                return 1;
            } else if (hasSmallerMinorThan(other)) {
                return -1;
            } else if (hasBiggerMinorThan(other)) {
                return 1;
            } else if (hasSmallerMicroThan(other)) {
                return -1;
            } else if (hasBiggerMicroThan(other)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            throw new IllegalArgumentException("Apples and oranges mate, apples and oranges");
        }
    }

    public boolean isNewerThan(final Version other) {
        if (this.compareTo(other)>0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isOlderThan(final Version other) {
        if (this.compareTo(other)<0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean qualifies(ArtifactQualifier o) {
        if (o !=null && o instanceof  Version) {
            Version other = (Version) o;
            if (other.name==Named.ANY)
                return true;
            else if (other.name == Named.LATEST)
                return true;
            if (other.major != null && this.major != other.major)
                return false;
            else if (other.minor != null && this.minor != other.minor)
                return false;
            else if (other.micro != null && this.micro != other.micro)
                return false;
            else
                return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (o == null || !(o instanceof Version)) {
            return false;
        }
        Version other = (Version) o;
        if (other.name != null && this.name != null && other.name.equals(this.name)) {
            return true;
        } else if (other.name == null && this.name == null && this.compareTo(other) == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, major, minor, micro);
    }

    public void fromString(final String paramString) {

        String groupRegex = "[0-9"+WILDCARD.toLowerCase()+WILDCARD.toUpperCase()+"]+";
        String allWildCardsRegex = "(["+WILDCARD.toLowerCase()+WILDCARD.toUpperCase()+"]+(\\.)*)+";
        String versionRegex = "("+groupRegex+"("+SEPARATOR_REGEX+")*)+";

        if (paramString == null || paramString.length() == 0 || paramString.matches(allWildCardsRegex)) {
            this.name = Named.ANY;
        } else if (Named.fromString(paramString.toUpperCase()) != null) {
            this.name = Named.fromString(paramString.toUpperCase());
        } else if (paramString.matches(versionRegex)) {
            String[] split = paramString.split(SEPARATOR_REGEX);
            major = split[0].toLowerCase().equals("x") ? null : Integer.parseInt(split[0]);
            if (split.length>1)
                minor = split[1].toLowerCase().equals("x") ? null : Integer.parseInt(split[1]);
            if (split.length>2)
                micro = split[2].toLowerCase().equals("x") ? null : Integer.parseInt(split[2]);
        }
    }
}
