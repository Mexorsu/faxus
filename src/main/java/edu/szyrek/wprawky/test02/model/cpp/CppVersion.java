package edu.szyrek.wprawky.test02.model.cpp;


import com.fasterxml.jackson.annotation.JsonTypeInfo;
import edu.szyrek.wprawky.test02.model.Version;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by wishm on 26/11/2016.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
@EqualsAndHashCode
public class CppVersion extends Version {
}
