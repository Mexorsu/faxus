package edu.szyrek.wprawky.test02.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import edu.szyrek.wprawky.test02.controller.Resolver;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by wishm on 26/11/2016.
 */
@EqualsAndHashCode
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public class ArtifactResolver extends ArtifactQualifier {

    @Getter
    @Setter
    @JsonProperty("class")
    public Class<? extends Resolver> clazz;

    @Override
    public boolean qualifies(ArtifactQualifier value) {
        return true;
    }

    @Override
    public int compareTo(Object o) {
        if (this.equals(o)) return 0;
        else return -1;
    }
}
