package edu.szyrek.wprawky.test02.model.java;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonValue;
import edu.szyrek.wprawky.test02.model.ArtifactId;
import edu.szyrek.wprawky.test02.model.ArtifactQualifier;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.Comparator;
import java.util.Objects;

/**
 * Created by wishm on 26/11/2016.
 */
@EqualsAndHashCode(callSuper = false)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public class JavaArtifactId extends ArtifactId {
    public static final String SEPARATOR = ":";
    @Getter
    @Setter
    private String groupID;

    @Getter
    @Setter
    private String artifactID;

    @JsonCreator
    public JavaArtifactId(String strThing)
    {
        String[] split = strThing.split(SEPARATOR);
        groupID = split[0];
        artifactID = split[1];
    }

    @Override
    @JsonValue
    public String toString()
    {
        return this.groupID + SEPARATOR + this.artifactID;
    }


    public JavaArtifactId() {}

    public JavaArtifactId(final String groupID, final String artifactID) {
        this.groupID = groupID;
        this.artifactID = artifactID;
    }

    @Override
    public String toPath() {
        StringBuffer buffer = new StringBuffer();
        for (String packageComponent: groupID.split("\\.")) {
            buffer.append(packageComponent);
            buffer.append(File.separator);
        }
        buffer.substring(0, buffer.length()-File.separator.length()-1);
        buffer.append(artifactID);
        buffer.append(File.separator);
        return buffer.toString();
    }

    @Override
    public String getName() {
        return (groupID + SEPARATOR + artifactID);
    }
}
