package edu.szyrek.wprawky.test02.model.cpp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonValue;
import edu.szyrek.wprawky.test02.model.ArtifactQualifier;
import edu.szyrek.wprawky.test02.model.ArtifactType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by wishm on 26/11/2016.
 */
@EqualsAndHashCode(callSuper = false)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public class CppArtifactType extends ArtifactType {
    @Getter
    @Setter
    private Type type;

    @JsonCreator
    public CppArtifactType(String strThing)
    {
        this.type = Type.valueOf(strThing.toUpperCase());
    }

    @Override
    @JsonValue
    public String toString()
    {
        return this.type.toString();
    }

    @Override
    public boolean qualifies(ArtifactQualifier value) {
        return this.equals(value);
    }

    @Override
    public String getExtension() {
        switch (type) {
            case DYNAMIC_LIB: return ".dll";
            case STATIC_LIB: return ".lib";
            case HEADER_LIB: return ".h";
            case EXECUTABLE: return ".exe";
            default: return "";
        }
    }

    @Override
    public String getContentType() {
        switch (type) {
            case DYNAMIC_LIB: return "application/lib-dynamic";
            case STATIC_LIB: return "application/lib-static";
            case HEADER_LIB: return "application/header";
            case EXECUTABLE: return "application/executable";
            default: return "application/text";
        }
    }


    public static enum Type {
        DYNAMIC_LIB, STATIC_LIB, HEADER_LIB, EXECUTABLE
    }
}
