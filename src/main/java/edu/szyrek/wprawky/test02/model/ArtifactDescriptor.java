package edu.szyrek.wprawky.test02.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by wishm on 26/11/2016.
 */
@Slf4j
public class ArtifactDescriptor {
    private final static String QUALIFIER_CLASS_SEPARATOR = ": ";
    private final static String QUALIFIERS_SEPARATOR = ", ";

    @Getter
    @Setter
    @JsonProperty("ArtifactDescriptor")
    private List<ArtifactQualifier> qualifiers = new ArrayList<>();

    public ArtifactDescriptor() {}
    public ArtifactDescriptor(ArtifactDescriptor other) {
        this.setQualifiers(other.getQualifiers());
    }

    public void addQualifier(ArtifactQualifier qualifier) {
        this.qualifiers.add(qualifier);
    }

    public <T extends ArtifactQualifier> boolean has(final Class<T> qualifierClass) {
        return qualifiers.stream()
                .filter(qualifier -> qualifierClass.isAssignableFrom(qualifier.getClass()))
                .collect(Collectors.counting()) > 0;
    }

    public <T extends ArtifactQualifier> boolean has(final Class<T> qualifierClass, final T value) {
        return has(qualifierClass) && all(qualifierClass).stream()
                .anyMatch(qualifier -> qualifier.equals(value));
    }

    public <T extends ArtifactQualifier> T get(final Class<T> qualifierClass) {
        List<T> sorted = new ArrayList<>();
        for (ArtifactQualifier qualifier: qualifiers) {
            if (qualifierClass.isAssignableFrom(qualifier.getClass())) {
                sorted.add((T)qualifier);
            }
        }
        sorted.sort(Version.comparator);

        return (sorted.size() > 0) ? sorted.get(0) : null;
    }

    public <T extends ArtifactQualifier> List<T> all(final Class<T> qualifierClass) {
        return qualifiers.stream()
                .filter(
                        qualifier ->
                                qualifier.getClass().isAssignableFrom(qualifierClass) ||
                                qualifierClass.isAssignableFrom(qualifier.getClass())
                )
                .map(
                        genericQualifier -> (T)genericQualifier
                )
                .sorted(ArtifactQualifier.comparator)
                .collect(Collectors.toList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(qualifiers);
    }


    @Override
    public boolean equals(Object o) {
        if (o == null || !this.getClass().isAssignableFrom(o.getClass())) {
            return false;
        }
        final ArtifactDescriptor other = (ArtifactDescriptor) o;
        if (this.qualifiers.size()!=other.qualifiers.size()) {
            return false;
        }
        for (final ArtifactQualifier qualifier: this.getQualifiers()) {
            if (!other.all(qualifier.getClass()).stream().anyMatch(otherQualifier->otherQualifier.equals(qualifier))) {
                return false;
            }
        }
        return true;
    }

    public static Comparator<ArtifactDescriptor> comparator = (o1, o2) -> {
        if (o1.has(Version.class) && o2.has(Version.class)) {
            return -o1.get(Version.class).compareTo(o2.get(Version.class));
        } else if (o1.has(Version.class)) {
            return 1;
        } else if (o2.has(Version.class)) {
            return -1;
        } else {
            return o1.qualifiers.size() == o2.qualifiers.size()
                    ? 0
                    : o1.qualifiers.size() > o2.qualifiers.size()
                    ? 1
                    : -1;
        }
    };

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        qualifiers.stream().forEach(
                qualifier -> buffer.append(qualifier.getClass().getSimpleName()+QUALIFIER_CLASS_SEPARATOR+qualifier.toString()+QUALIFIERS_SEPARATOR)
        );
        return (buffer.length() > 0) ? "ArtifactDesc: "+ buffer.toString().substring(0, buffer.length()-QUALIFIERS_SEPARATOR.length()) : "ArtifactDesc: empty";
    }


}
