package edu.szyrek.wprawky.test02.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.EqualsAndHashCode;

/**
 * Created by wishm on 27/11/2016.
 */

@EqualsAndHashCode(callSuper = false)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public abstract class ArtifactType extends ArtifactQualifier {
    @Override
    public int compareTo(Object o) {
        if (this.equals(o))
            return 0;
        else
            return this.hashCode()-o.hashCode();
    }

    public abstract String getExtension();

    public abstract String getContentType();
}
