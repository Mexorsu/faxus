package edu.szyrek.wprawky.test02.model.java;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonValue;
import edu.szyrek.wprawky.test02.model.ArtifactQualifier;
import edu.szyrek.wprawky.test02.model.ArtifactType;
import lombok.EqualsAndHashCode;

/**
 * Created by wishm on 27/11/2016.
 */
@EqualsAndHashCode
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public class JavaArtifactType extends ArtifactType {
    private Type type;

    @JsonCreator
    public JavaArtifactType(String strThing)
    {
        this.type = JavaArtifactType.Type.valueOf(strThing.toUpperCase());
    }

    public JavaArtifactType(Type type)
    {
        this.type = type;
    }


    @Override
    @JsonValue
    public String toString()
    {
        return this.type.toString();
    }

    @Override
    public boolean qualifies(ArtifactQualifier value) {
        return this.equals(value);
    }

    @Override
    public String getExtension() {
        switch (type) {
            case JAR:
            case EXECUTABLE_JAR: return "jar";
            case WAR: return "war";
            case EAR: return "ear";
            case CLASS: return "class";
            case JAVA: return "java";
            case XML: return "xml";
            default: return "";
        }
    }

    @Override
    public String getContentType() {
        switch (type) {
            case JAR:
            case EXECUTABLE_JAR: return "application/jar";
            case WAR: return "application/jar";
            case EAR: return "application/ear";
            case CLASS: return "application/class";
            case JAVA: return "application/java";
            case XML: return "application/xml";
            default: return "text/xml";
        }
    }
    public enum Type {
        JAR, EXECUTABLE_JAR, WAR, EAR, CLASS, JAVA, XML
    }


}
