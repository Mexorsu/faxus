package edu.szyrek.wprawky.test02.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by wishm on 26/11/2016.
 */
public class Artifact {

    private static final Random random = new SecureRandom();
    private static final String charset = "qwertyuioplkjhgfdsazxcvbnm1234567890";

    private static String getRandomString(final int length) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < length; i++) {
            buffer.append(charset.charAt(random.nextInt(charset.length())));
        }
        return buffer.toString();
    }

    @Getter
    @Setter
    @NotNull
    private String id;

    @Getter
    @Setter
    private Path path;

    @Getter
    @Setter
    private boolean resolved;

    @Getter
    private ArtifactDescriptor descriptor;

    public Artifact(ArtifactDescriptor descriptor) {
        this.id = getRandomString(32);
        this.descriptor = descriptor;
    }
    public void resolve(final Path resolvedPath) throws IOException {
        this.resolved = true;
        this.path = resolvedPath;
    }


}
