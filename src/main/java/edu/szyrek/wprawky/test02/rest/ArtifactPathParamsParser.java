package edu.szyrek.wprawky.test02.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.szyrek.wprawky.test02.model.ArtifactId;
import edu.szyrek.wprawky.test02.model.ArtifactQualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * Created by wishm on 27/11/2016.
 */
@Configuration
public class ArtifactPathParamsParser {
    private final ObjectMapper json;

    @Autowired
    public ArtifactPathParamsParser(final ObjectMapper json) {
        this.json = json;
    }

    public <T extends ArtifactQualifier> T parseQualifier(final String stringValue, final String implementationName, final Class<T> qualifierInterface) throws IOException {
        final String qualifierJson = "{\""+implementationName+"\":\""+stringValue+"\"}";
        return json.readValue(qualifierJson, qualifierInterface);
    }
}
