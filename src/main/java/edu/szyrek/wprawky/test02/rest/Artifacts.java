package edu.szyrek.wprawky.test02.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.szyrek.wprawky.test02.controller.ArtifactRepository;
import edu.szyrek.wprawky.test02.controller.DefaultArtifactRepository;
import edu.szyrek.wprawky.test02.controller.ResolverInstantiationError;
import edu.szyrek.wprawky.test02.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

/**
 * Created by wishm on 26/11/2016.
 */
@Slf4j
@RestController
@Configuration
@Import({ArtifactRepository.class, DefaultArtifactRepository.class})
public class Artifacts {
    private final ArtifactRepository repository;
    private final ArtifactPathParamsParser paramsParser;

    @Autowired
    public Artifacts(final ArtifactRepository repository, final ArtifactPathParamsParser paramsParser ) {
        this.repository = repository;
        this.paramsParser = paramsParser;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/artifacts")
    public List<ArtifactDescriptor> getArtifacts() {
        return repository.getAll();
    }


    @RequestMapping(method = RequestMethod.POST, path = "/artifacts")
    public List<ArtifactDescriptor> find(@RequestBody final ArtifactDescriptor filter) {
        return repository.filter(filter);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/artifact")
    public void findOne(@RequestBody final ArtifactDescriptor filter, final HttpServletResponse response) {
        ArtifactDescriptor concrete = repository.get(filter);
        writeArtifactFileToResponseOutputStream(concrete, response);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/artifact")
    public void createArtifact(@RequestBody final ArtifactDescriptor artifact) {
        repository.add(artifact);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/artifacts")
    public void createArtifacts(@RequestBody final List<ArtifactDescriptor> artifacts) {
        for (final ArtifactDescriptor artifact: artifacts) {
            repository.add(artifact);
        }
    }

    @RequestMapping(method = RequestMethod.GET, path = "/artifact/{idClass}/{idString}/")
    public void getArtifact(
            @PathVariable final String idClass,
            @PathVariable final String idString,
                            final HttpServletResponse response) throws IOException {
        final ArtifactDescriptor filterDescriptor =  new ArtifactDescriptor();

        filterDescriptor.addQualifier(paramsParser.parseQualifier(idString, idClass, ArtifactId.class));
        filterDescriptor.addQualifier(Version.LATEST);

        final ArtifactDescriptor fullDescriptor = repository.get(filterDescriptor);

        writeArtifactFileToResponseOutputStream(fullDescriptor, response);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/artifact/{idClass}/{idString}/{versionClass}/{versionString}")
    public void getArtifact(
            @PathVariable final String idClass,
            @PathVariable final String idString,
            @PathVariable final String versionClass,
            @PathVariable final String versionString,
            final HttpServletResponse response) throws IOException {
        final ArtifactDescriptor filterDescriptor =  new ArtifactDescriptor();

        filterDescriptor.addQualifier(paramsParser.parseQualifier(idString, idClass, ArtifactId.class));
        filterDescriptor.addQualifier(paramsParser.parseQualifier(versionString, versionClass, Version.class));

        final ArtifactDescriptor fullDescriptor = repository.get(filterDescriptor);

        writeArtifactFileToResponseOutputStream(fullDescriptor, response);
    }




//    @RequestMapping(method = RequestMethod.GET, path = "/artifact/{id}/{version}")
//    public void getArtifact(@PathVariable final String id,
//                                @PathVariable final String version,
//                                final HttpServletResponse response) throws IOException {
//
//        ArtifactId artifactId = jacksonObjectMapper.readValue(id, ArtifactId.class);
//        Version ver = new Version(version);
//        ArtifactDescriptor descriptor = new ArtifactDescriptor();
//        descriptor.addQualifier(artifactId);
//        descriptor.addQualifier(ver);
//
//        ArtifactDescriptor latest = repository.getOne(descriptor);
//
//        writeArtifactFileToResponseOutputStream(latest, response);
//
//    }

//    @RequestMapping(method = RequestMethod.GET, path = "/artifact/{id}/{version}/{qualifier}")
//    public void getArtifact(
//        @PathVariable final String id,
//        @PathVariable final String version,
//        @PathVariable final String qualifier,
//        final HttpServletResponse response) throws IOException {
//
//        ArtifactId artifactId = jacksonObjectMapper.readValue(id, ArtifactId.class);
//        Version ver = new Version(version);
//        JavaQualifier artifactQualifier = new JavaQualifier(qualifier);
//        ArtifactDescriptor descriptor = new ArtifactDescriptor();
//        descriptor.addQualifier(artifactId);
//        descriptor.addQualifier(ver);
//        descriptor.addQualifier(artifactQualifier);
//
//        ArtifactDescriptor latest = repository.getOne(descriptor);
//
//        writeArtifactFileToResponseOutputStream(latest, response);
//    }
//
//    @RequestMapping(method = RequestMethod.GET, path = "/artifact/{id}/{version}/{qualifier}/{type}")
//    public void getArtifact(
//            @PathVariable final String id,
//            @PathVariable final String version,
//            @PathVariable final String qualifier,
//            @PathVariable final String type,
//            final HttpServletResponse response) throws IOException {
//
//        final ArtifactId artifactId = jacksonObjectMapper.readValue(id, ArtifactId.class);
//        final Version artifactVersion = new Version(version);
//        final JavaQualifier artifactQualifier = new JavaQualifier(qualifier);
//        final ArtifactType artifactType = jacksonObjectMapper.readValue(type, ArtifactType.class);
//
//        final ArtifactDescriptor descriptor = new ArtifactDescriptor();
//        descriptor.addQualifier(artifactId);
//        descriptor.addQualifier(artifactVersion);
//        descriptor.addQualifier(artifactQualifier);
//        descriptor.addQualifier(artifactType);
//
//        ArtifactDescriptor latest = repository.getOne(descriptor);
//
//        writeArtifactFileToResponseOutputStream(latest, response);
//    }


    private void writeArtifactFileToResponseOutputStream(final ArtifactDescriptor descriptor, final HttpServletResponse response) {
        try {
            // get your file as InputStream
            InputStream is = Files.newInputStream(repository.fetch(descriptor).getPath());
            // set content meta-info
            response.addHeader("content-disposition", "attachment; filename="+descriptor.get(ArtifactId.class).getName()+"."+descriptor.get(ArtifactType.class).getExtension());
            response.setContentType(descriptor.get(ArtifactType.class).getContentType());
            response.setContentLength(is.available());
            // copy it to response's OutputStream
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            log.error("Error while writing {}", descriptor, ex);
            throw new RuntimeException("IOError writing file to output stream");
        } catch (ResolverInstantiationError resolverInstantiationError) {
            resolverInstantiationError.printStackTrace();
        }
    }

}
