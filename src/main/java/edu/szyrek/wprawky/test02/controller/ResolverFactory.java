package edu.szyrek.wprawky.test02.controller;

/**
 * Created by wishm on 26/11/2016.
 */
public class ResolverFactory {


    public static <T extends Resolver> T getInstance(Class<T> resolverClass) throws ResolverInstantiationError {
        try {
            return resolverClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ResolverInstantiationError(resolverClass, e);
        }
    }

}
