package edu.szyrek.wprawky.test02.controller;

import edu.szyrek.wprawky.test02.model.ArtifactDescriptor;

import java.nio.file.Path;

/**
 * Created by wishm on 26/11/2016.
 */
public interface Resolver {
    public Path resolve(final ArtifactDescriptor descriptor) throws ResolverError;
}
