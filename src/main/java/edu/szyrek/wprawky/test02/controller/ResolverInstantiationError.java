package edu.szyrek.wprawky.test02.controller;

import edu.szyrek.wprawky.test02.model.ArtifactDescriptor;

/**
 * Created by wishm on 26/11/2016.
 */
public class ResolverInstantiationError extends Exception {
    public ResolverInstantiationError(final Class<? extends Resolver> desc, final String errorMsg) {
        super("Failed to instantiate resolver " + desc + ": " + errorMsg);
    }
    public ResolverInstantiationError(final Class<? extends Resolver> desc, final Throwable cause) {
        super("Failed to instantiate resolver " + desc + ": " + cause.getMessage());
    }
}
