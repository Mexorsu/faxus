package edu.szyrek.wprawky.test02.controller;

import edu.szyrek.wprawky.test02.model.*;
import edu.szyrek.wprawky.test02.model.java.JavaArtifactId;
import edu.szyrek.wprawky.test02.model.java.JavaArtifactType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by wishm on 26/11/2016.
 */
@Slf4j
@Configuration
public class DefaultArtifactRepository implements ArtifactRepository {
    final Set<Resolver> artifactResolvers = new HashSet<>();

    private List<ArtifactDescriptor> descriptors = new ArrayList<>();

    public DefaultArtifactRepository() {
        ArtifactDescriptor test = new ArtifactDescriptor();
        test.addQualifier(new JavaArtifactId("edu.szyrek.wprawky","faxus"));
        test.addQualifier(new Version(0,2,1));
        test.addQualifier(new JavaQualifier("SNAPSHOT"));
        test.addQualifier(new JavaArtifactType(JavaArtifactType.Type.JAR));
        descriptors.add(test);
        this.artifactResolvers.add(new LocalFIleSystemResolver());
    }

    @Override
    public void add(final ArtifactDescriptor descriptor) {
        descriptors.add(descriptor);
    }

    @Override
    public List<ArtifactDescriptor> getAll() {
        return descriptors;
    }

    @Override
    public ArtifactDescriptor get(final ArtifactDescriptor descriptor) {
        final List<ArtifactDescriptor> results = filter(descriptor);

        if (results.size()==0) {
            throw new NoSuchElementException("Artifact with descriptor "+descriptor.toString()+" not found");
        } else if(results.size() > 1 && descriptor.has(Version.class, Version.LATEST)) {
            results.sort(ArtifactDescriptor.comparator);
        } else if(results.size() > 1 && descriptor.has(Version.class, Version.ANY)) {
            // do nothing, don't sort they wanted any version- they will get any version
        } else if(results.size() > 1) {
            throw new NoSuchElementException("Too loose descriptor: "+descriptor.toString()+". mutliple artifacts found");
        }
        return results.get(0);
    }

    @Override
    public List<ArtifactDescriptor> filter(ArtifactDescriptor descriptor) {
        Stream<ArtifactDescriptor> stream = descriptors.stream();

        for (final ArtifactQualifier filter: descriptor.getQualifiers()) {
            stream = stream.filter(artifact-> artifact.all(filter.getClass()).stream().anyMatch(qualifier -> qualifier.qualifies(filter)));
        }
        return stream.sorted(ArtifactDescriptor.comparator).collect(Collectors.toList());
    }

    @Override
    public ArtifactDescriptor getLatest(ArtifactDescriptor descriptor) {
        return filter(descriptor).stream().min(ArtifactDescriptor.comparator).get();
    }

    private Path tryFetch(final ArtifactDescriptor descriptor, final Resolver resolver) {
        try {
            return resolver.resolve(descriptor);
        } catch (final ResolverError error) {
            log.error("Failed to resolve {} from {}", descriptor, resolver, error);
            return null;
        }
    }



    @Override
    @Cacheable("descriptors")
    public Artifact fetch(final ArtifactDescriptor descriptor) throws ResolverInstantiationError, IOException {
        final Set<Resolver> resolvers = new HashSet<>();

        final Artifact artifact = new Artifact(descriptor);

        if (descriptor.has(ArtifactResolver.class)) {
            resolvers.add(ResolverFactory.getInstance(((ArtifactResolver)descriptor.get(ArtifactResolver.class)).getClazz()));
        } else {
            resolvers.addAll(artifactResolvers);
        }
        for (final Resolver resolver: resolvers) {
            final Path resolvedPath = tryFetch(descriptor, resolver);
            artifact.resolve(resolvedPath);
        }
        return artifact;
    }
}
