package edu.szyrek.wprawky.test02.controller;

import edu.szyrek.wprawky.test02.model.Artifact;
import edu.szyrek.wprawky.test02.model.ArtifactDescriptor;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.List;

/**
 * Created by wishm on 27/11/2016.
 */

@Configuration
public interface ArtifactRepository {
    List<ArtifactDescriptor> getAll();
    void add(final ArtifactDescriptor descriptor);
    ArtifactDescriptor get(ArtifactDescriptor descriptor);
    List<ArtifactDescriptor> filter(ArtifactDescriptor descriptor);
    ArtifactDescriptor getLatest(ArtifactDescriptor descriptor);
    Artifact fetch(final ArtifactDescriptor descriptor) throws ResolverInstantiationError, IOException;
}
