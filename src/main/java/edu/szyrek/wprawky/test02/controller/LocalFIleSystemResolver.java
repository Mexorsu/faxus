package edu.szyrek.wprawky.test02.controller;

import edu.szyrek.wprawky.test02.model.*;

import java.lang.System;
import java.nio.file.Path;
import java.io.File;
import java.nio.file.Paths;

/**
 * Created by wishm on 26/11/2016.
 */
public class LocalFIleSystemResolver implements Resolver {

    private static final Path repo_root = Paths.get(
            System.getProperty("faxus.repo",
                    System.getProperty("user.home")+ File.separator + ".faxus" + File.separator
            )
    );

    @Override
    public Path resolve(ArtifactDescriptor descriptor) throws ResolverError {
        final String folder = repo_root.toString() + File.separator + descriptor.get(ArtifactId.class).toPath();
        final String version = descriptor.get(Version.class).toString() +File.separator;
        final String qualifierComponent = descriptor.has(JavaQualifier.class) ?
                descriptor.get(JavaQualifier.class).toString() + File.separator :
                "";
        final String fileName = descriptor.get(ArtifactId.class).getName();
        final String extension = descriptor.get(ArtifactType.class).getExtension();

        final String fullPath = folder + version + qualifierComponent + fileName + "." + extension;

        return Paths.get(fullPath);
    }
}
