package edu.szyrek.wprawky.test02.controller;

import edu.szyrek.wprawky.test02.model.Artifact;
import edu.szyrek.wprawky.test02.model.ArtifactDescriptor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wishm on 26/11/2016.
 */
public class ArtifactCache {
    // TODO: yeah, this ;)
    private Map<ArtifactDescriptor, Artifact> cache = new HashMap<>();
    private DefaultArtifactRepository repository;

    public ArtifactCache(final DefaultArtifactRepository repo) {
        this.repository = repo;
    }

    public Artifact fetch(final ArtifactDescriptor key) throws ResolverInstantiationError, IOException {
        return fetch(key, repository);
    }

    public Artifact fetch(final ArtifactDescriptor key, final DefaultArtifactRepository repository) throws ResolverInstantiationError, IOException {
        if (cache.containsKey(key)) {
            return cache.get(key);
        } else {
            final Artifact newEntry = repository.fetch(key);
            cache.put(key, newEntry);
            return newEntry;
        }
    }
}
