package edu.szyrek.wprawky.test02.controller;

import edu.szyrek.wprawky.test02.model.ArtifactDescriptor;

/**
 * Created by wishm on 26/11/2016.
 */
public class ResolverError extends Exception {
    public ResolverError(final ArtifactDescriptor desc, final String errorMsg) {
        super("Failed to resolve " + desc + ": " + errorMsg);
    }
    public ResolverError(final ArtifactDescriptor desc, final Throwable cause) {
        super("Failed to resolve " + desc + ": " + cause.getMessage());
    }
}
